import axios from '@/libs/api.request'
import store from '@/store/index';

//创建页面属性
export const createPageConfig = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/pageconfig/insert',
    data,
    method: 'post'
  })
}
//获取页面数据
//middle/shopconfig/pageconfig/get
export const getPageData = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/pageconfig/get',
    data,
    method: 'post'
  })
}
//创建页面数据
///middle/shopconfig/pagedata/update
export const updatePageConfig = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/pageconfig/insert',
    data,
    method: 'post'
  })
}

//获取商品列表
///middle/shopconfig/pagedata/update
export const getProductList = opts => {
  let data = opts
  return axios.request({
    url: '/middle/product/queryProducts',
    data,
    method: 'post'
  })
}

//获取页面列表
//middle/shopconfig/pageconfig/page
export const getPageList = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/pageconfig/page',
    data,
    method: 'post'
  })
}
//删除页面列表
//middle/shopconfig/pageconfig/page
export const delPageList = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/pageconfig/delete',
    data,
    method: 'post'
  })
}
//设置为默认首页
//middle/shopconfig/indexpageconfig/changeindexpage
export const setDefaultPage = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/indexpageconfig/changeindexpage',
    data,
    method: 'post'
  })
}
//删除计划首页
export const deletePlanPage = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/indexpageconfig/delete',
    data,
    method: 'post'
  })
}
///middle/auth/dologout
export const doLogout = opts => {
  let data = opts
  return axios.request({
    url: '/middle/auth/dologout',
    data,
    method: 'post'
  })
}
//查询首页计划列表
export const getPlanPageList = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/indexpageconfig/page',
    data,
    method: 'post'
  })
}

//新添首页计划
export const addPlanPage = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/indexpageconfig/insert',
    data,
    method: 'post'
  })
}
//新添加导航
export const addPageguid = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/pageguiddata/insert',
    data,
    method: 'post'
  })
}
///middle/shopconfig/pageglobalconfig/insert
//保存页面全局配置
export const savePageGlobal = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/pageglobalconfig/insert',
    data,
    method: 'post'
  })
}
//更新页面数据
//middle/shopconfig/pageconfig/update
export const pageconfigupdate = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/pageconfig/update',
    data,
    method: 'post'
  })
}
//页面分类列表
export const getCateList = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/pagecate/list',
    data,
    method: 'post'
  })
}
//middle/shopconfig/pagecate/insert
//新建页面分类
export const createCata = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/pagecate/insert',
    data,
    method: 'post'
  })
}
///middle/shopconfig/pagecate/delete
//删除页面分类
export const delCata = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/pagecate/delete',
    data,
    method: 'post'
  })
}
//middle/shopconfig/pageconfig/updatepagecate
//批量更新页面分类
export const updatepagecate = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/pageconfig/updatepagecate',
    data,
    method: 'post'
  })
}
//查询一级分类 /middle/product/queryFirstProductCatesByCustomerId
export const getLevel1 = opts => {
  let data = opts
  return axios.request({
    url: '/middle/product/queryFirstProductCatesByCustomerId',
    data,
    method: 'post'
  })
}

//查询二级分类 /middle/product/querySecondProductCatesByPid
export const getLevel2 = opts => {
  let data = opts
  return axios.request({
    url: '/middle/product/querySecondProductCatesByPid',
    data,
    method: 'post'
  })
}
//查询三级分类 /middle/product/querySecondProductCatesByPid
export const getLevel3 = opts => {
  let data = opts
  return axios.request({
    url: '/middle/product/queryThirdProductCatesByCIdAndPId',
    data,
    method: 'post'
  })
}
//查询活动三级分类
//middle/order/stages/queryStagesThirdCatesByCIdAndPId
export const getActiveLevel3 = opts => {
  let data = opts
  return axios.request({
    url: '/middle/order/stages/queryStagesThirdCatesByCIdAndPId',
    data,
    method: 'post'
  })
}
//查询所有素材
//middle/material/group/all
export const getAllMaterial = opts => {
  let data = opts
  return axios.request({
    url: '/middle/material/group/all',
    data,
    method: 'post'
  })
}
///middle/material/detail/delete
export const delMaterial = opts => {
  let data = opts
  return axios.request({
    url: '/middle/material/detail/delete',
    data,
    method: 'post'
  })
}
///middle/material/detail/create
//创建图片素材
export const addMaterial = opts => {
  let data = opts
  return axios.request({
    url: '/middle/material/detail/create',
    data,
    method: 'post'
  })
}
///middle/product/queryProducts
export const getProducts = opts => {
  let data = opts
  return axios.request({
    url: '/middle/product/queryProducts',
    data,
    method: 'post'
  })
}
//查品牌
//middle/product/queryProductBrandsByTypeId
export const getBrands = opts => {
  let data = opts
  return axios.request({
    url: '/middle/product/queryProductBrandsByCustomerId',
    data,
    method: 'post'
  })
}
//创建素材分组
export const createMaterial = opts => {
  let data = opts
  return axios.request({
    url: '/middle/material/group/create',
    data,
    method: 'post'
  })
}
//查询分组中的图片素材
///middle/material/group/detail
export const getMaterialImgs = opts => {
  let data = opts
  return axios.request({
    url: '/middle/material/group/detail',
    data,
    method: 'post'
  })
}
//费率活动列表
//middle/order/stages/list
export const orderStages = opts => {
  let data = opts
  return axios.request({
    url: '/middle/order/stages/list',
    data,
    method: 'post'
  })
}
///middle/order/stages/updateState
//费率列表状态更改 状态 1.新建 2.启用 3.停用 4.删除
export const upOrderState = opts => {
  let data = opts
  return axios.request({
    url: '/middle/order/stages/updateState',
    data,
    method: 'post'
  })
}
////middle/order/stages/update
//费率列表状态更改 状态 1.新建 2.启用 3.停用 4.删除
export const stagesState = opts => {
  let data = opts
  return axios.request({
    url: '/middle/order/stages/update',
    data,
    method: 'post'
  })
}
//middle/order/stages/toEdit
//费率编辑查询
export const editOrderState = opts => {
  let data = opts
  return axios.request({
    url: '/middle/order/stages/toEdit',
    data,
    method: 'post'
  })
}
//middle/order/stages/create
//生成活动
export const createRate = opts => {
  let data = opts
  return axios.request({
    url: '/middle/order/stages/create',
    data,
    method: 'post'
  })
}
//保存费率活动
///middle/order/stages/product/create
export const saveRateActive = opts => {
  let data = opts
  return axios.request({
    url: '/middle/order/stages/product/create',
    data,
    method: 'post'
  })
}
//查询活动商品列表
//middle/order/stages/product/list/search
export const activeProductList = opts => {
  let data = opts
  return axios.request({
    url: '/middle/order/stages/product/list/search',
    data,
    method: 'post'
  })
}

//查询已选活动商品列表
///middle/order/stages/product/list
export const selectActiveProductList = opts => {
  let data = opts
  return axios.request({
    url: '/middle/order/stages/product/list',
    data,
    method: 'post'
  })
}
//middle/order/stages/product/createProduct
//添加商品到已选列表
export const addSelectProducts = opts => {
  let data = opts
  return axios.request({
    url: '/middle/order/stages/product/createProduct',
    data,
    method: 'post'
  })
}
///middle/order/stages/product/delete
//删除商品到已选列表
export const delSelectProducts = opts => {
  let data = opts
  return axios.request({
    url: '/middle/order/stages/product/delete',
    data,
    method: 'post'
  })
}

//middle/commodity/aftersaleconfig/list
//售后设置列表
export const afterSellList = opts => {
  let data = opts
  return axios.request({
    url: '/middle/commodity/aftersaleconfig/list',
    data,
    method: 'post'
  })
}
//获取默认售后配置
export const aftersaleconfig = opts => {
  let data = opts
  return axios.request({
    url: '/middle/commodity/aftersaleconfig/loaddefaultconfig',
    data,
    method: 'post'
  })
}
//middle/commodity/aftersaleconfig/insert
//新增售后配置
export const insertaftersaleconfig = opts => {
  let data = opts
  return axios.request({
    url: '/middle/commodity/aftersaleconfig/insert',
    data,
    method: 'post'
  })
}
//middle/commodity/aftersaleconfig/delete
//删除售后配置
export const delaftersaleconfig = opts => {
  let data = opts
  return axios.request({
    url: '/middle/commodity/aftersaleconfig/delete',
    data,
    method: 'post'
  })
}
//middle/commodity/aftersaleconfig/update
//更新售后
export const updateaftersaleconfig = opts => {
  let data = opts
  return axios.request({
    url: '/middle/commodity/aftersaleconfig/update',
    data,
    method: 'post'
  })
}
//middle/system/user/get
//获取用户手机号
export const getUserPhone = opts => {
  let data = opts
  return axios.request({
    url: '/middle/system/user/get',
    data,
    method: 'post'
  })
}
//middle/sms/send
//获取验证码
export const getSms = opts => {
  let data = opts
  return axios.request({
    url: '/middle/sms/send',
    data,
    method: 'post'
  })
}
//middle/sms/check
//校验验证码
export const checkSms = opts => {
  let data = opts
  return axios.request({
    url: '/middle/sms/check',
    data,
    method: 'post'
  })
}
//middle/system/user/updatepassword
//修改密码
export const updatepassword = opts => {
  let data = opts
  return axios.request({
    url: '/middle/system/user/updatepassword',
    data,
    method: 'post'
  })
}
//middle/system/user/updateMobile
//修改手机号
export const updateMobile = opts => {
  let data = opts
  return axios.request({
    url: '/middle/system/user/updateMobile',
    data,
    method: 'post'
  })
}
//middle/shopconfig/findCustomerMsg
//获取商城信息
export const getMallMsg = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/findCustomerMsg',
    data,
    method: 'post'
  })
}
//middle/shopconfig/findFirstLoginConfig
//获取商城信息
export const getMallInitMsg = opts => {
  let data = opts
  return axios.request({
    url: '/middle/shopconfig/findFirstLoginConfig',
    data,
    method: 'post'
  })
}
//middle/customer/updateInfo
export const updateInfo = opts => {
  let data = opts
  return axios.request({
    url: '/middle/customer/updateInfo',
    data,
    method: 'post'
  })
}
//公用
export const getTableData = opts => {
  return axios.request({
    headers: { Authorization: store.state.user.token },
    url: opts.url,
    data: opts.data,
    method: opts.type
  })
}






/*
  ================================================================
 */
// export const getTableData = opts => {
//   return axios.request({
//     url: opts.url,
//     data:opts.data,
//     method: 'post'
//   })
// }

export const getDragList = () => {
  return axios.request({
    url: 'get_drag_list',
    method: 'get'
  })
}

export const errorReq = () => {
  return axios.request({
    url: 'error_url',
    method: 'post'
  })
}

export const saveErrorLogger = info => {
  return axios.request({
    url: 'save_error_logger',
    data: info,
    method: 'post'
  })
}

export const uploadImg = formData => {
  return axios.request({
    url: 'image/upload',
    data: formData
  })
}

export const getOrgData = () => {
  return axios.request({
    url: 'get_org_data',
    method: 'get'
  })
}

export const getTreeSelectData = () => {
  return axios.request({
    url: 'get_tree_select_data',
    method: 'get'
  })
}
export const doLogin = opts =>{
  return axios.request({
    url: 'http://10.1.5.163:8083/admin/auth/dologin',
    data:opts.data,
    method: 'post'
  })
}

