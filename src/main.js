// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import iView from 'iview'
import i18n from '@/locale'
import config from '@/config'
import importDirective from '@/directive'
import { directive as clickOutside } from 'v-click-outside-x'
import installPlugin from '@/plugin'
import './index.less'
import './assets/normalize.css'
import './assets/self.css'
import '@/assets/icons/iconfont.css'
import TreeTable from 'tree-table-vue'
import VOrgTree from 'v-org-tree'
import 'v-org-tree/dist/v-org-tree.css'
import tools from './assets/js/arms.es6'
import lodash from 'lodash'
Vue.prototype.$tools = tools
// 实际打包时应该不引入mock
/* eslint-disable */
if (process.env.NODE_ENV !== 'production') require('@/mock')

Vue.use(iView, {
  i18n: (key, value) => i18n.t(key, value)
})
Vue.use(TreeTable)
Vue.use(VOrgTree)
/**
 * @description 注册admin内置插件
 */
installPlugin(Vue)
/**
 * @description 生产环境关掉提示
 */
Vue.config.productionTip = false
/**
 * @description 全局注册应用配置
 */
Vue.prototype.$config = config
//Vue.prototype.$publicUrl='http://10.1.10.80:8081'
//Vue.prototype.$publicUrl = 'http://10.1.3.80:8083/'
Vue.prototype.$publicUrl = 'http://test-jdd-server.juzifenqi.cn/api/'
// Vue.prototype.$publicApp = store.state.user.dominUrl+'/webapp/main/activity.html?type=1&pageId='
// Vue.prototype.$publicClassifyUrl=store.state.user.dominUrl+'/webapp/main/category.html?type=1&categoryId='
//Vue.prototype.$publicUrl = 'http://test-jdd-server.juzifenqi.cn/api/'
/**
 * 注册指令
 */
importDirective(Vue)
Vue.directive('clickOutside', clickOutside)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  i18n,
  store,
  render: h => h(App)
})
