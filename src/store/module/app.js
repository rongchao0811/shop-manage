import {
  getBreadCrumbList,
  setTagNavListInLocalstorage,
  getMenuByRouter,
  getTagNavListFromLocalstorage,
  getHomeRoute,
  getNextRoute,
  routeHasExist,
  routeEqual,
  getRouteTitleHandled,
  localSave,
  localRead
} from '@/libs/util'
import { saveErrorLogger } from '@/api/data'
import router from '@/router'
import routers from '@/router/routers'
import config from '@/config'
const { homeName } = config

const closePage = (state, route) => {
  const nextRoute = getNextRoute(state.tagNavList, route)
  state.tagNavList = state.tagNavList.filter(item => {
    return !routeEqual(item, route)
  })
  router.push(nextRoute)
}

export default {
  state: {
    breadCrumbList: [],
    tagNavList: [],
    homeRoute: {},
    local: localRead('local'),
    errorList: [],
    hasReadErrorPage: false,
    createPageData:[{
        type:"overall",
        editType:"add",
        pageTitle:'',
        pageBg:'',
        pageBgColor:'',
        pageClassify:[],
        pageDesc:'',
        index:0,
        navColumn:'',
        topColumn:'',
        productBrandIdArray:[],//产品品牌集合
        productClassIdArray:[],//产品分类集合
      }],//
    mouseTarget:{},
    classifyPageData:[],//分类页数据
    classifyMouseTarget:{},
    classifyListData:{
      pageAttr:Object,
      productBrandIdArray:[],
      productClassIdArray:[],
      list:[{list:[]}]
    },//分类页列表
    materialListData:[],//分类页数据
    rubikMouseIndex:0,
    classRubikMouseIndex:0,
    currentPage:{}
  },
  getters: {
    menuList: (state, getters, rootState) => getMenuByRouter(routers, rootState.user.access),
    errorCount: state => state.errorList.length
  },
  mutations: {
    setBreadCrumb (state, route) {
      state.breadCrumbList = getBreadCrumbList(route, state.homeRoute)
    },
    setHomeRoute (state, routes) {
      state.homeRoute = getHomeRoute(routes, homeName)
    },
    setTagNavList (state, list) {
      let tagList = []
      if (list) {
        tagList = [...list]
      } else tagList = getTagNavListFromLocalstorage() || []
      if (tagList[0] && tagList[0].name !== homeName) tagList.shift()
      let homeTagIndex = tagList.findIndex(item => item.name === homeName)
      if (homeTagIndex > 0) {
        let homeTag = tagList.splice(homeTagIndex, 1)[0]
        tagList.unshift(homeTag)
      }
      state.tagNavList = tagList
      setTagNavListInLocalstorage([...tagList])
    },
    closeTag (state, route) {
      let tag = state.tagNavList.filter(item => routeEqual(item, route))
      route = tag[0] ? tag[0] : null
      if (!route) return
      closePage(state, route)
    },
    addTag (state, { route, type = 'unshift' }) {
      let router = getRouteTitleHandled(route)
      if (!routeHasExist(state.tagNavList, router)) {
        if (type === 'push') state.tagNavList.push(router)
        else {
          if (router.name === homeName) state.tagNavList.unshift(router)
          else state.tagNavList.splice(1, 0, router)
        }
        setTagNavListInLocalstorage([...state.tagNavList])
      }
    },
    setLocal (state, lang) {
      localSave('local', lang)
      state.local = lang
    },
    addError (state, error) {
      state.errorList.push(error)
    },
    setHasReadErrorLoggerStatus (state, status = true) {
      state.hasReadErrorPage = status
    },
    setCurrentPage(state,v){
      state.currentPage = v
    },
    setClassifyMouseTarget(state,v){
      state.classifyMouseTarget = v
    },
    setMouseTarget(state,v){
      state.mouseTarget = v
    },
    setMaterialPageData(state,v){
      if(v.editType=='add'){
        state.materialListData.push(v)
      }
    },
    setClassRubikMouseIndex(state,v){
      state.classRubikMouseIndex = v
    },
    setRubikMouseIndex(state,v){
      state.rubikMouseIndex = v
    },
    setClassifyList(state,v){
      if(v.classType == 'attr'){
        state.classifyListData.pageAttr = v.attr
      }else if(v.classType == 'list' && v.editType == 'add'){
        //state.classifyListData.list
        state.classifyListData.list=state.classifyListData.list.filter((item)=>{
          return item.classifyName
        })
        state.classifyListData.list.push(v)
      }else if(v.classType == 'list' && v.editType == 'change'){
        state.classifyListData.list[v.targetIndex] = v
      }else if(v.classType == 'innerList' && v.editType == 'add'){
        // alert(v.targetIndex)
        // alert(JSON.stringify(state.classifyListData.list))
        state.classifyListData.list[v.targetIndex].list.push(v)
      }else if(v.classType == 'classList'){
        state.classifyListData.productClassIdArray=v.classList
      }else if(v.classType == 'brandList'){
        state.classifyListData.productBrandIdArray=v.brandList
      }else if(v.classType=='backData'){
        // state.classifyListData.pageAttr=v.pageAttr
        state.classifyListData.list = v.list
      }else if(v.classType=='reset'){
        state.classifyListData={
          pageAttr:Object,
          productBrandIdArray:[],
          productClassIdArray:[],
          list:[{list:[]}]
        }
      }
    },
    setClassifyPageData(state,v){
      if(v.type == 'overall'){
        state.classifyPageData[0] = v
      }else if(v.editType == 'add'){
        state.classifyPageData.push(v)
      }else if(v.editType == 'remove'){
        console.log('remove')
        state.classifyPageData = state.classifyPageData.filter((item)=>{
          return item.index != v.index
        })
        console.log('分类页删除后数据==='+JSON.stringify( state.classifyPageData))
      }else if(v.editType == 'change'){
        console.log('change')
        state.classifyPageData[v.index] = v
        console.log('分类页修改后的数据=='+JSON.stringify(state.classifyPageData))
      }
    },
    setPageData(state,v){
      console.log(v.type)
      if(v.type == 'overall'){
        state.createPageData[0] = v
      }else if(v.editType == 'add'){
        state.createPageData.push(v)
      }else if(v.editType == 'remove'){
        console.log('remove')
        state.createPageData = state.createPageData.filter((item)=>{
          return item.index != v.index
        })
        console.log('删除后数据==='+JSON.stringify( state.createPageData))
      }else if(v.editType == 'change'){
        console.log('change')
        // let temp = _.fill(state.createPageData,v,v.index)
        state.createPageData[v.index] = v
        console.log('修改后的数据=='+JSON.stringify(state.createPageData))
      }else if (v.editType == 'reset'){
        state.createPageData = [{"type":"overall","editType":"add",
          "rubikType":"onePic",pageTitle:'标题',pageBg:'',
          pageBgColor:'',
          pageClassify:[],pageDesc:'',
          productBrandIdArray:[],//产品品牌集合
          productClassIdArray:[],//产品分类集合
          "index":0,navColumn:'',topColumn:''}]
      }else if(v.editType == 'backData'){
        state.createPageData = v
      }
    }
  },
  actions: {
    addErrorLog ({ commit, rootState }, info) {
      if (!window.location.href.includes('error_logger_page')) commit('setHasReadErrorLoggerStatus', false)
      const { user: { token, userId, userName } } = rootState
      let data = {
        ...info,
        time: Date.parse(new Date()),
        token,
        userId,
        userName
      }
      // saveErrorLogger(info).then(() => {
      //   commit('addError', data)
      // })
    }
  }
}
