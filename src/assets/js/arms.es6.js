export default {
  //类型判断
  isArray(obj) {
      return Object.prototype.toString.call(obj) === '[object Array]';
    },
    isObject(obj) {
      return Object.prototype.toString.call(obj) === '[object Object]';
    },
    isNumber(obj) {
      return Object.prototype.toString.call(obj) === '[object Number]';
    },
    isString(obj) {
      return Object.prototype.toString.call(obj) === '[object String]';
    },
    isFunction(obj) {
      return Object.prototype.toString.call(obj) === '[object Function]';
    },
    isBoolean(obj) {
      return Object.prototype.toString.call(obj) === '[object Boolean]';
    },
    isNull(obj) {
      return obj == null || obj == '' || obj == undefined;
    },
    isNotNull(obj) {
      return !this.isNull(obj);
    },
    //取范围内随机数
    randomNum(min, max) {
      return Math.floor(min + Math.random() * (max - min));
    },
    phoneReg: {
      //中国电信号码段
      CHINA_TELECOM_PATTERN: /^(?:\+86)?1(?:33|53|7[37]|8[019])\d{8}$|^(?:\+86)?1700\d{7}$/,
      //中国联通号码段
      CHINA_UNICOM_PATTERN: /^(?:\+86)?1(?:3[0-2]|4[5]|5[56]|7[56]|8[56])\d{8}$|^(?:\+86)?170[7-9]\d{7}$/,
      //中国移动号码段
      CHINA_MOBILE_PATTERN: /^(?:\+86)?1(?:3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478])\d{8}$|^(?:\+86)?1705\d{7}$/,
      //电话座机号码段
      PHONE_CALL_PATTERN: /^(?:\(\d{3,4}\)|\d{3,4}-)?\d{7,8}(?:-\d{1,4})?$/,
      //手机号码
      PHONE_PATTERN: /^(?:\+86)?(?:13\d|14[57]|15[0-35-9]|17[35-8]|18\d)\d{8}$|^(?:\+86)?170[057-9]\d{7}$/,
      //手机号简单校验，不根据运营商分类
      PHONE_SIMPLE_PATTERN: /^(?:\+86)?1\d{10}$/
    },
    //座机电话号码 true/false
    // isPhoneNum(input) {
    //   return this.phoneReg.PHONE_CALL_PATTERN.test(input);
    // },
    //电信手机号码
    isChinaTelecomPhoneNum(input) {
      return this.phoneReg.CHINA_TELECOM_PATTERN.test(input);
    },
    //中国联通
    isChinaUnicomPhoneNum(input) {
      return this.phoneReg.CHINA_UNICOM_PATTERN.test(input);
    },
    //中国移动
    isChinaMobilePhoneNum(input) {
      return this.phoneReg.CHINA_MOBILE_PATTERN.test(input);
    },
    //手机号码
    isPhoneNum(input) {
      return this.phoneReg.PHONE_PATTERN.test(input);
    },
    //手机号码简单校验，只校验长度
    isPhoneNumBySize(input) {
      return this.phoneReg.PHONE_SIMPLE_PATTERN.test(input);
    },
    //邮箱正则
    EMAIL_PATTERN: /^[-\w\+]+(?:\.[-\w]+)*@[-a-z0-9]+(?:\.[a-z0-9]+)*(?:\.[a-z]{2,})$/i,
    //邮箱格式校验
    isEmail(input) {
      return this.EMAIL_PATTERN.test(input);
    },
    //判断空 不包括空格
    isEmpty(input) {
      return input == null || input == '';
    },
    //判断非空
    isNotEmpty(input) {
      return !this.isEmpty(input);
    },
    //判断空，包括空格
    isBlank(input) {
      return input == null || /^\s*$/.test(input);
    },
    //判断空，如果只有空格，也算有内容
    isNotBlank(input) {
      return !this.isBlank(input);
    },
    //去空格
    trim(input) {
      return input.replace(/^\s+|\s+$/, '');
    },
    //去空格，去null
    trimToEmpty(input) {
      return input == null ? "" : this.trim(input);
    },
    //检查是否包含
    contains(input, searchSeq) {
      return input.indexOf(searchSeq) >= 0;
    },
    //是否相等
    equals(input1, input2) {
      return input1 == input2;
    },
    //是否相等 （不区分大小写）
    equalsIgnoreCase(input1, input2) {
      return input1.toLocaleLowerCase() == input2.toLocaleLowerCase();
    },
    //是否包含空格
    containsWhitespace(input) {
      return this.contains(input, ' ');
    },
    //特殊字符校验
    hasSpecial(input){
      return /[~!@#$%^&*()/\|,.<>?"'();:_+-=\[\]{}]/.test(input)
    },
    //首小写字母转大写
    capitalize(input) {
      var strLen = 0;
      if (input == null || (strLen = input.length) == 0) {
        return input;
      }
      return input.replace(/^[a-z]/, (matchStr) => {
        return matchStr.toLocaleUpperCase();
      });
    },
    //统计含有的子字符串的个数
    countMatches(input, sub) {
      if (this.isEmpty(input) || this.isEmpty(sub)) {
        return 0;
      }
      var count = 0;
      var index = 0;
      while ((index = input.indexOf(sub, index)) != -1) {
        index += sub.length;
        count++;
      }
      return count;
    },
    //只包含字母
    isAlpha(input) {
      return /^[a-z]+$/i.test(input);
    },
    //只包含字母、空格
    isAlphaSpace(input) {
      return /^[a-z\s]*$/i.test(input);
    },
    //只包含字母、数字
    isAlphanumeric(input) {
      return /^[a-z0-9]+$/i.test(input);
    },
    //只包含字母、数字和空格
    isAlphanumericSpace(input) {
      return /^[a-z0-9\s]*$/i.test(input);
    },
    //数字
    isNumeric(input) {
      return /^(?:[1-9]\d*|0)(?:\.\d+)?$/.test(input);
    },
    //小数
    isDecimal(input) {
      return /^[-+]?(?:0|[1-9]\d*)\.\d+$/.test(input);
    },
    //负小数
    isNegativeDecimal(input) {
      return /^\-(?:0|[1-9]\d*)\.\d+$/.test(input);
    },
    //正小数
    isPositiveDecimal(input) {
      return /^\+?(?:0|[1-9]\d*)\.\d+$/.test(input);
    },
    //整数
    isInteger(input) {
      return /^[-+]?(?:0|[1-9]\d*)$/.test(input);
    },
    //正整数
    isPositiveInteger(input) {
      return /^\+?(?:0|[1-9]\d*)$/.test(input);
    },
    //负整数
    isNegativeInteger(input) {
      return /^\-(?:0|[1-9]\d*)$/.test(input);
    },
    //只包含数字和空格
    isNumericSpace(input) {
      return /^[\d\s]*$/.test(input);
    },
    //删掉特殊字符(英文状态下)
    removeSpecialCharacter(input) {
      return input.replace(/[!-/:-@\[-`{-~]/g, "");
    },
    //中文校验
    isChinese(input) {
      return /^[\u4E00-\u9FA5]+$/.test(input);
    },
    //去掉中文字符
    removeChinese(input) {
      return input.replace(/[\u4E00-\u9FA5]+/gm, "");
    },
    //转义元字符
    escapeMetacharacter(input) {
      var metacharacter = "^$()*+.[]|\\-?{}";
      if (metacharacter.indexOf(input) >= 0) {
        input = "\\" + input;
      }
      return input;
    },
    idCardRegex: {
      //18位身份证简单校验
      IDCARD_18_SIMPLE_PATTERN: /^(?:1[1-5]|2[1-3]|3[1-7]|4[1-6]|5[0-4]|6[1-5])\d{4}(?:1[89]|20)\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])\d{3}(?:\d|[xX])$/,
      //15位身份证简单校验
      IDCARD_15_SIMPLE_PATTERN: /^(?:1[1-5]|2[1-3]|3[1-7]|4[1-6]|5[0-4]|6[1-5])\d{4}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])\d{3}$/
    },
    //18位身份证简单校验
    isSimpleIdCard18(idCard) {
      return this.idCardRegex.IDCARD_18_SIMPLE_PATTERN.test(idCard);
    },
    //15位身份证简单校验
    isSimpleIdCard15(idCard) {
      return this.idCardRegex.IDCARD_18_SIMPLE_PATTERN.test(idCard);
    },
    //18位身份证校验码校验
    checkCode(idCard) {
      var multiplier = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
      var idDatas = idCard.split("");
      var len = 17;
      var sum = 0;
      for (var i = 0; i < len; i++) {
        sum += idDatas[i] * multiplier[i];
      }
      var remainder = sum % 11;
      var checkCodeArr = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'];
      var checkCode = checkCodeArr[remainder];
      return checkCode === idCard[17];
    },
    //18位身份证严格校验
    isIdCard18(idCard) {
      //先校验格式
      if (this.isSimpleIdCard18(idCard)) {
        //校验日期时间是否合法
        var dateStr = idCard.substr(6, 8);
        var dateStrNew = dateStr.replace(/(\d{4})(\d{2})(\d{2})/, "$1/$2/$3");
        var dateObj = new Date(dateStrNew);
        var month = dateObj.getMonth() + 1;
        if (parseInt(dateStr.substr(4, 2)) === month) {
          return this.checkCode(idCard);
        }
      }
      return false;
    },
    //根据18身份证号码获取人员信息
    getPersonInfo18(idCard) {
      var age = 0;
      var birthday = '';
      var address = '';
      var sex = '';
      address = Bee.areas[idCard.substr(0, 2) + '0000'] + ' ' + Bee.areas[idCard.substr(0, 4) + '00'] + ' ' + Bee.areas[idCard.substr(0, 6)];
      sex = (idCard.substr(16, 1) % 2 === 0) ? '女' : '男';
      birthday = idCard.substr(6, 8).replace(/(\d{4})(\d{2})(\d{2})/, '$1年$2月$3日');
      age = new Date().getFullYear() - idCard.substr(6, 4) + 1;
      var person = {
        'address': address,
        'sex': sex,
        'birthday': birthday,
        'age': age
      };
      return person;
    },
    isNest(rule, str) {
      if (!(rule && str)) {
        return false;
      }
      var keys = [];
      var values = [];
      for (var key in rule) {
        if (rule.hasOwnProperty(key)) {
          keys.push(key);
          values.push(rule[key]);
        }
      }
      var chs = str.split("");
      var len = chs.length;
      var stack = [];
      for (var i = 0; i < len; i++) {
        if (Bee.ArrayUtils.inArray(keys, chs[i])) {
          stack.push(rule[chs[i]]);
        } else {
          if (chs[i] === stack[stack.length - 1]) {
            stack.pop();
          } else if (Bee.ArrayUtils.inArray(values, chs[i])) {
            return false;
          }
        }
      }
      return stack.length === 0;
    },
    /**
     *
     * @desc 判断两个数组是否相等
     * @param {Array} arr1
     * @param {Array} arr2
     * <a href='http://www.jobbole.com/members/wx1409399284'>@return</a> {Boolean}
     */
    arrayEqual(arr1, arr2) {
      if (arr1 === arr2) return true;
      if (arr1.length != arr2.length) return false;
      for (var i = 0; i < arr1.length; ++i) {
        if (arr1[i] !== arr2[i]) return false;
      }
      return true;
    },
    //获取数组最大值
    getMaxValue(arr) {
      return Math.max.apply(Math, arr);
    },
    //获取数组最小值
    getMinValue(arr) {
      return Math.min.apply(Math, arr);
    },
    //判断某个值是否在数组
    inArray(arr, ele) {
      var len = arr.length;
      for (var i = 0; i < len; i++) {
        if (ele === arr[i]) {
          return true;
        }
      }
      return false;
    },
    //删除数组中一个元素
    remove(arr, ele) {
      var index = arr.indexOf(ele);
      if (index > -1) {
        arr.splice(index, 1);
      }
      return arr;
    },
    //将类数组转换为数组
    formArray(ary) {
      var arr = [];
      if (Array.isArray(ary)) {
        arr = ary;
      } else {
        arr = Array.prototype.slice.call(ary);
      };
      return arr;
    },
    /*最大值*/
    max(arr) {
      return Math.max.apply(null, arr);
    },
    /*最小值*/
    min(arr) {
      return Math.min.apply(null, arr);
    },
    /*求和*/
    sum(arr) {
      return arr.reduce((pre, cur) => {
        return pre + cur
      })
    },
    /*平均值*/
    average(arr) {
      return this.sum(arr) / arr.length
    },
    /**
     * @desc 获取访问路径中某个参数
     * @param paramName 参数名
     * @param url 指定要截取参数的url（可以为空，如果为空url指向当前页面）
     */
    getParamter(paramName, url) {
      var seachUrl = window.location.search.replace("?", "");
      if (url != null) {
        var index = url.indexOf('?');
        url = url.substr(index + 1);
        seachUrl = url;
      }
      var ss = seachUrl.split("&");
      var paramNameStr = "";
      var paramNameIndex = -1;
      for (var i = 0; i < ss.length; i++) {
        paramNameIndex = ss[i].indexOf("=");
        paramNameStr = ss[i].substring(0, paramNameIndex);
        if (paramNameStr == paramName) {
          var returnValue = ss[i].substring(paramNameIndex + 1, ss[i].length);
          if (typeof(returnValue) == "undefined") {
            returnValue = "";
          }
          return returnValue;
        }
      }
      return "";
    },
    //检测密码强度
    checkPwd(str) {
      var Lv = 0;
      if (str.length < 6) {
        return Lv
      }
      if (/[0-9]/.test(str)) {
        Lv++
      }
      if (/[a-z]/.test(str)) {
        Lv++
      }
      if (/[A-Z]/.test(str)) {
        Lv++
      }
      if (/[\.|-|_]/.test(str)) {
        Lv++
      }
      return Lv;
    },
    /*将阿拉伯数字翻译成中文的大写数字*/
    numberToChinese(num) {
      var AA = new Array("零", "一", "二", "三", "四", "五", "六", "七", "八", "九", "十");
      var BB = new Array("", "十", "百", "仟", "萬", "億", "点", "");
      var a = ("" + num).replace(/(^0*)/g, "").split("."),
        k = 0,
        re = "";
      for (var i = a[0].length - 1; i >= 0; i--) {
        switch (k) {
          case 0:
            re = BB[7] + re;
            break;
          case 4:
            if (!new RegExp("0{4}//d{" + (a[0].length - i - 1) + "}$")
              .test(a[0]))
              re = BB[4] + re;
            break;
          case 8:
            re = BB[5] + re;
            BB[7] = BB[5];
            k = 0;
            break;
        }
        if (k % 4 == 2 && a[0].charAt(i + 2) != 0 && a[0].charAt(i + 1) == 0)
          re = AA[0] + re;
        if (a[0].charAt(i) != 0)
          re = AA[a[0].charAt(i)] + BB[k % 4] + re;
        k++;
      }
      if (a.length > 1) // 加上小数部分(如果有小数部分)
      {
        re += BB[6];
        for (var i = 0; i < a[1].length; i++)
          re += AA[a[1].charAt(i)];
      }
      if (re == '一十')
        re = "十";
      if (re.match(/^一/) && re.length == 3)
        re = re.replace("一", "");
      return re;
    },
    /*将数字转换为大写金额*/
    changeToChinese(Num) {
      //判断如果传递进来的不是字符的话转换为字符
      if (typeof Num == "number") {
        Num = new String(Num);
      };
      Num = Num.replace(/,/g, "") //替换tomoney()中的“,”
      Num = Num.replace(/ /g, "") //替换tomoney()中的空格
      Num = Num.replace(/￥/g, "") //替换掉可能出现的￥字符
      if (isNaN(Num)) { //验证输入的字符是否为数字
        //alert("请检查小写金额是否正确");
        return "";
      };
      //字符处理完毕后开始转换，采用前后两部分分别转换
      var part = String(Num).split(".");
      var newchar = "";
      //小数点前进行转化
      for (var i = part[0].length - 1; i >= 0; i--) {
        if (part[0].length > 10) {
          return "";
          //若数量超过拾亿单位，提示
        }
        var tmpnewchar = ""
        var perchar = part[0].charAt(i);
        switch (perchar) {
          case "0":
            tmpnewchar = "零" + tmpnewchar;
            break;
          case "1":
            tmpnewchar = "壹" + tmpnewchar;
            break;
          case "2":
            tmpnewchar = "贰" + tmpnewchar;
            break;
          case "3":
            tmpnewchar = "叁" + tmpnewchar;
            break;
          case "4":
            tmpnewchar = "肆" + tmpnewchar;
            break;
          case "5":
            tmpnewchar = "伍" + tmpnewchar;
            break;
          case "6":
            tmpnewchar = "陆" + tmpnewchar;
            break;
          case "7":
            tmpnewchar = "柒" + tmpnewchar;
            break;
          case "8":
            tmpnewchar = "捌" + tmpnewchar;
            break;
          case "9":
            tmpnewchar = "玖" + tmpnewchar;
            break;
        }
        switch (part[0].length - i - 1) {
          case 0:
            tmpnewchar = tmpnewchar + "元";
            break;
          case 1:
            if (perchar != 0) tmpnewchar = tmpnewchar + "拾";
            break;
          case 2:
            if (perchar != 0) tmpnewchar = tmpnewchar + "佰";
            break;
          case 3:
            if (perchar != 0) tmpnewchar = tmpnewchar + "仟";
            break;
          case 4:
            tmpnewchar = tmpnewchar + "万";
            break;
          case 5:
            if (perchar != 0) tmpnewchar = tmpnewchar + "拾";
            break;
          case 6:
            if (perchar != 0) tmpnewchar = tmpnewchar + "佰";
            break;
          case 7:
            if (perchar != 0) tmpnewchar = tmpnewchar + "仟";
            break;
          case 8:
            tmpnewchar = tmpnewchar + "亿";
            break;
          case 9:
            tmpnewchar = tmpnewchar + "拾";
            break;
        }
        var newchar = tmpnewchar + newchar;
      }
      //小数点之后进行转化
      if (Num.indexOf(".") != -1) {
        if (part[1].length > 2) {
          // alert("小数点之后只能保留两位,系统将自动截断");
          part[1] = part[1].substr(0, 2)
        }
        for (i = 0; i < part[1].length; i++) {
          tmpnewchar = ""
          perchar = part[1].charAt(i)
          switch (perchar) {
            case "0":
              tmpnewchar = "零" + tmpnewchar;
              break;
            case "1":
              tmpnewchar = "壹" + tmpnewchar;
              break;
            case "2":
              tmpnewchar = "贰" + tmpnewchar;
              break;
            case "3":
              tmpnewchar = "叁" + tmpnewchar;
              break;
            case "4":
              tmpnewchar = "肆" + tmpnewchar;
              break;
            case "5":
              tmpnewchar = "伍" + tmpnewchar;
              break;
            case "6":
              tmpnewchar = "陆" + tmpnewchar;
              break;
            case "7":
              tmpnewchar = "柒" + tmpnewchar;
              break;
            case "8":
              tmpnewchar = "捌" + tmpnewchar;
              break;
            case "9":
              tmpnewchar = "玖" + tmpnewchar;
              break;
          }
          if (i == 0) tmpnewchar = tmpnewchar + "角";
          if (i == 1) tmpnewchar = tmpnewchar + "分";
          newchar = newchar + tmpnewchar;
        }
      }
      //替换所有无用汉字
      while (newchar.search("零零") != -1)
        newchar = newchar.replace("零零", "零");
      newchar = newchar.replace("零亿", "亿");
      newchar = newchar.replace("亿万", "亿");
      newchar = newchar.replace("零万", "万");
      newchar = newchar.replace("零元", "元");
      newchar = newchar.replace("零角", "");
      newchar = newchar.replace("零分", "");
      if (newchar.charAt(newchar.length - 1) == "元") {
        newchar = newchar + "整"
      }
      return newchar;
    },
    /*检测类名*/
    hasClass(ele, name) {
      return ele.className.match(new RegExp('(\\s|^)' + name + '(\\s|$)'));
    },
    /*添加类名*/
    addClass(ele, name) {
      if (!this.hasClass(ele, name)) ele.className += " " + name;
    },
    /*删除类名*/
    removeClass(ele, name) {
      if (this.hasClass(ele, name)) {
        var reg = new RegExp('(\\s|^)' + name + '(\\s|$)');
        ele.className = ele.className.replace(reg, '');
      }
    },
    /*替换类名*/
    replaceClass(ele, newName, oldName) {
      this.removeClass(ele, oldName);
      this.addClass(ele, newName);
    },
    /**
     *
     * @desc 获取滚动条距顶部的距离
     */
    getScrollTop() {
      return (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
    },
    /**
     * @desc 设置滚动条距顶部的距离
     */
    setScrollTop(value) {
      window.scrollTo(0, value);
      return value;
    },
    /**
     * @desc 获取操作系统类型
     * @return {String}
     */
    getOS() {
      var userAgent = 'navigator' in window && 'userAgent' in navigator && navigator.userAgent.toLowerCase() || '';
      var vendor = 'navigator' in window && 'vendor' in navigator && navigator.vendor.toLowerCase() || '';
      var appVersion = 'navigator' in window && 'appVersion' in navigator && navigator.appVersion.toLowerCase() || '';
      if (/mac/i.test(appVersion)) return 'MacOSX'
      if (/win/i.test(appVersion)) return 'windows'
      if (/linux/i.test(appVersion)) return 'linux'
      if (/iphone/i.test(userAgent) || /ipad/i.test(userAgent) || /ipod/i.test(userAgent)) 'ios'
      if (/android/i.test(userAgent)) return 'android'
      if (/win/i.test(appVersion) && /phone/i.test(userAgent)) return 'windowsPhone'
    },
    //获取浏览器类型和版本
    getExplore() {
      var sys = {},
        ua = navigator.userAgent.toLowerCase(),
        s;
      (s = ua.match(/rv:([\d.]+)\) like gecko/)) ? sys.ie = s[1]:
        (s = ua.match(/msie ([\d\.]+)/)) ? sys.ie = s[1] :
        (s = ua.match(/edge\/([\d\.]+)/)) ? sys.edge = s[1] :
        (s = ua.match(/firefox\/([\d\.]+)/)) ? sys.firefox = s[1] :
        (s = ua.match(/(?:opera|opr).([\d\.]+)/)) ? sys.opera = s[1] :
        (s = ua.match(/chrome\/([\d\.]+)/)) ? sys.chrome = s[1] :
        (s = ua.match(/version\/([\d\.]+).*safari/)) ? sys.safari = s[1] : 0;
      // 根据关系进行判断
      if (sys.ie) return ('IE: ' + sys.ie)
      if (sys.edge) return ('EDGE: ' + sys.edge)
      if (sys.firefox) return ('Firefox: ' + sys.firefox)
      if (sys.chrome) return ('Chrome: ' + sys.chrome)
      if (sys.opera) return ('Opera: ' + sys.opera)
      if (sys.safari) return ('Safari: ' + sys.safari)
      return 'Unkonwn'
    },
}
